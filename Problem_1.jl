### A Pluto.jl notebook ###
# v0.17.1

using Markdown
using InteractiveUtils

# ╔═╡ 7818cd54-b03d-4180-a364-b66784739de7
begin
	import Pkg;
	Pkg.add("DataFrames")
	Pkg.add("Shuffle")
	Pkg.add("CSV")
	Pkg.add("Plots")
	Pkg.add("Lathe")
	Pkg.add("GLM")
	Pkg.add("StatsPlots")
	Pkg.add("MLBase")
	Pkg.add("Flux")
	Pkg.add("Tracker")
end

# ╔═╡ 191ffc1e-bbd8-44b9-a0c7-f3659142818e
begin
	using DataFrames
	using Shuffle
	using CSV
	using Plots
	using Lathe
	using GLM
	using Statistics
	using StatsPlots
	using MLBase
	using Flux
	using Flux: throttle
	using Tracker
end

# ╔═╡ 6fe3ff57-e30b-4ab5-9e8f-66646ca4ee96
md" # Installing and Importing the required packages"

# ╔═╡ e034b031-982a-4b1f-b9a3-db3c768b3e2d
md" # 1. importing the csv data file"

# ╔═╡ 4fa2b078-6e96-43cb-a15e-2bce9e101d16
data = CSV.read("C://Users//Elifas//Documents//Documents//Artificial Intilegence//Real estate.csv",DataFrame)

# ╔═╡ f072f217-dc35-4918-9cdd-eb992010bdad
md" # 2. Data Exploration"

# ╔═╡ 1c8dbef3-eb15-4b8d-9888-63633d8bc016
size(data)

# ╔═╡ c6f9182e-0f77-438f-93ce-d7579920a007
describe(data)

# ╔═╡ c4ca48e9-536f-4dec-8140-ee8557e61c55
names(data)

# ╔═╡ 68e529b5-e4cd-4a91-9e5d-4bff7313294a
# Box plot
boxplot(data.X3_distance_to_the_nearest_MRT_station,title="Box plot house age",ylabel="House age(years)",legend=false)

# ╔═╡ d2f8495e-4ee6-4ea1-bdd1-1c9f30cae90c
# Density plot
density(data.X3_distance_to_the_nearest_MRT_station,title="Density plot",ylabel="Frequency",xlabel="Distance to nearest station",legend=false)

# ╔═╡ 52d53228-f3bb-4835-9c9c-37cb9296b58c
md" # 3. Cleaning the data"

# ╔═╡ a0460e88-39ce-4586-a6e3-9caa416925c6
# 1. Renaming columns names

# ╔═╡ 8f731102-0028-4242-8f4a-8e2edd892ac7
begin
		rename!(data, Symbol.(replace.(string.(names(data)), Ref(r"\[m\]"=>"_"))))
		rename!(data, Symbol.(replace.(string.(names(data)), Ref(r"\[s\]"=>"_"))))
		rename!(data, Symbol.(replace.(string.(names(data)), Ref(r"\s"=>"_"))))
end

# ╔═╡ ded31cf5-eebf-4d68-bd7f-71554797f30b
# Rounding house age to whole number
round.(data.X2_house_age,digits=0)

# ╔═╡ 5e96e8f4-c6a1-4eeb-9a4e-7e9bec6c85f8
md"# Building the Linear Regression model"

# ╔═╡ d993744a-8ab4-423c-8807-d5c2b225173e
#Selecting the required data for the model
begin
	Xdata = data[:,:X3_distance_to_the_nearest_MRT_station]
	Ydata = data[:,:Y_house_price_of_unit_area]
end

# ╔═╡ b84ce479-b8a5-4bac-b430-d61dfe39366c
begin
	
	df = zip(Xdata,Ydata)
	
	df = gpu.(df)
	
	# Defining the model
	model = Dense(1,1,identity) |> gpu
	
	# Utilizing the mean square error loss
	loss(x,y) = Flux.mse(model([x]),y)
	
	# Using callback ftn to display the loss
	call_back_ftn = () -> @show(sum([loss(i[1],i[2]) for i in 
	df]))
	
	#Using Stocastic Gradient Descent to optmize model weights
	optmize = Descent(0.1)
	
	# Training the model
	for i = 1:length(df)
		Flux.train!(loss,params(model),df,optmize,cb=throttle(call_back_ftn,10))
	end
	
end

# ╔═╡ 9206641f-6939-475a-9589-a7c4d078511d
(θ,bias) = Tracker.data.(Flux.params(model))

# ╔═╡ 1d7d6291-cc0b-40b2-a62e-184ad974d1a5
plot!((x) -> bias[1] + θ[1] * x,0,1,ylabel="Distance to nearest station",xlabel="House price",label="Fit_flux")

# ╔═╡ Cell order:
# ╠═6fe3ff57-e30b-4ab5-9e8f-66646ca4ee96
# ╠═7818cd54-b03d-4180-a364-b66784739de7
# ╠═191ffc1e-bbd8-44b9-a0c7-f3659142818e
# ╠═e034b031-982a-4b1f-b9a3-db3c768b3e2d
# ╠═4fa2b078-6e96-43cb-a15e-2bce9e101d16
# ╠═f072f217-dc35-4918-9cdd-eb992010bdad
# ╠═1c8dbef3-eb15-4b8d-9888-63633d8bc016
# ╠═c6f9182e-0f77-438f-93ce-d7579920a007
# ╠═c4ca48e9-536f-4dec-8140-ee8557e61c55
# ╠═68e529b5-e4cd-4a91-9e5d-4bff7313294a
# ╠═d2f8495e-4ee6-4ea1-bdd1-1c9f30cae90c
# ╠═52d53228-f3bb-4835-9c9c-37cb9296b58c
# ╠═a0460e88-39ce-4586-a6e3-9caa416925c6
# ╠═8f731102-0028-4242-8f4a-8e2edd892ac7
# ╠═ded31cf5-eebf-4d68-bd7f-71554797f30b
# ╠═5e96e8f4-c6a1-4eeb-9a4e-7e9bec6c85f8
# ╠═d993744a-8ab4-423c-8807-d5c2b225173e
# ╠═b84ce479-b8a5-4bac-b430-d61dfe39366c
# ╠═9206641f-6939-475a-9589-a7c4d078511d
# ╠═1d7d6291-cc0b-40b2-a62e-184ad974d1a5
