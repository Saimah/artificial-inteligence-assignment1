### A Pluto.jl notebook ###
# v0.19.0

using Markdown
using InteractiveUtils

# ╔═╡ 6c8afd46-e038-4e50-bcbe-964768d7014b
#Installing the required packages
begin
	import Pkg;
	Pkg.add("MLJFlux")
	Pkg.add("Plots")
	Pkg.add("Images")
	Pkg.add("ImageFeatures")
	Pkg.add("ImageMagick")
	Pkg.add("PyCall")
	Pkg.add("TestImages")
	Pkg.add("MLDataUtils")
	Pkg.add("MLDatasets")
	Pkg.add("CSV")
	Pkg.add("Zygote")
	Pkg.add("CUDA")
	Pkg.add("ProgressMeter")
	Pkg.add("ImageView")
	Pkg.add("FileIO")
	Pkg.add("BSON")
	Pkg.add("JpegTurbo")
	Pkg.instantiate()
end

# ╔═╡ 37766d90-b434-11ec-17f6-4f777d3d2564
begin
	using Flux.Data.MNIST: Gray
	using Flux, Flux.Data.MNIST,Statistics
	using Flux: onehotbatch, onecold, crossentropy, throttle
	using Base.Iterators: repeated, partition
	using BSON
	using FileIO
	using JpegTurbo
	using PlutoUI
	using Flux
	using CSV
	using Zygote
	using MLDatasets
	using MLDataUtils
	using Images,ImageFeatures,ImageMagick
	using MLJFlux
	using Plots
	using CUDA
	using Flux:Data.DataLoader
	using Flux: @epochs
	using Flux.Optimise: Optimiser, WeightDecay
	using Flux.Losses: logitcrossentropy
	using ProgressMeter: @showprogress
	using Statistics, Random
	using DataFrames
	using ImageView
	using Printf
	using FastAI
	import BSON
end

# ╔═╡ 31034f1d-aa87-4bb9-b93c-db662ee7d27a
# Defining the image paths
begin
	train_image_path ="../chest_xray/train"
	test_image_path ="../chest_xraay/test"
	val_image_path ="../chest_xray/val"
end

# ╔═╡ 43489228-8517-411c-9cd7-e4ae876cfe7f
# Loading images into the program
begin
	normal_image_train = "$train_image_path\\NORMAL\\"
	normal_image_train_data = readdir(normal_image_train)
	normal_image_test = "$test_image_path\\NORMAL\\"
	normal_image_test_data = readdir(normal_image_test)
	normal_image_val = "$valid_image_path\\NORMAL\\"
	normal_image_val_data = readdir(normal_image_val)
	pneumonia_image_train = "$train_image_path\\PNEUMONIA\\"
	pneumonia_image_train_data = readdir(pneumonia_image_train)
	pneumonia_image_test = "$test_image_path\\PNEUMONIA\\"
	pneumonia_image_test_data = readdir(pneumonia_image_test)
	pneumonia_image_val = "$valid_image_path\\PNEUMONIA\\"
	pneumonia_image_val_data = readdir(pneumonia_image_val)
end

# ╔═╡ 8ad31f6d-ff3f-495e-99f3-4a6d60a221b7
# Setting batch size for iteration training
begin
	n_pos_train = length(readdir(normal_image_train))
	n_pos_test = length(readdir(normal_image_test))
	n_pos_val = length(readdir(normal_image_val))
	p_neg_train =length(readdir(pneumonia_image_train))
	p_neg_test =length(readdir(pneumonia_image_test))
	p_neg_val =length(readdir(pneumonia_image_val))
	train_total = n_pos_train + p_neg_train
	test_total = n_pos_test + p_neg_test
	val_total = n_pos_val + p_neg_val
end

# ╔═╡ 6d57ba6c-c336-4ccb-ac19-23b8f61d4c1f
# Defining image width and height
begin
	img_height = 200
	img_width = 200
end

# ╔═╡ ab465a7c-faa0-4ebd-8a72-7c5054b8d437
# Train and test function
function train_test_data(mode)
	if mode == 0
		dataset_normal = normal_val.*normal_val_data
		dataset_pneumonia = pneumonia_val.*pneumonia_val_data
	elseif mode == 1
		dataset_normal = normal_test.*normal_test_data
		dataset_pneumonia = pneumonia_test.*pneumonia_test_data
	elseif mode == 2
		dataset_normal = normal_train.*normal_train_data
		dataset_pneumonia = pneumonia_train.*pneumonia_train_data
	end
	#combines both negative dataset as well as positive data set
	dataset = [dataset_normal; dataset_pneumonia]
	return dataset
end

# ╔═╡ 1feca470-10ce-4ebe-b703-aab3cbec5ca1
# Utilising train test data function
begin
	img_paths = DataFrame(img = train_test_data(2))
	img2_paths = DataFrame(img = train_test_data(1))
	img3_paths = DataFrame(img = train_test_data(0))
end

# ╔═╡ 9c2aadfa-a581-436e-9160-815baa883394
# Creating two classes to generate testing and training data
begin
	classes = ["Normal", "Pneumonia"]
	all_Classes = CSV.read("Lables.csv",DataFrame)
	
	norm_Class = CSV.read("Normal.csv",DataFrame)
	x_pos = size(norm_Class[!,1])
		
	pneum_Class = CSV.read("Pneumonia.csv",DataFrame)
	x_neg = size(pneum_Class[!,1])
end

# ╔═╡ 0cc2fd45-b87e-4fda-90dd-ffa537a375e9
# Getting the training and testing data
begin
	x_train, y_train = img_paths, all_Classes

	dataSet = hcat(x_train, y_train)
	
	x_dataset = [ [row.img, row.Classes] for row in eachrow(dataSet)]
end

# ╔═╡ bef0c8aa-2de3-47fd-96c5-08e3b0cb4ea6
# Image resize function
function resizeImage(img)
	img_small = imresize(img, (100,100))#ratio=1/8)
	return img_small
end


# ╔═╡ eec55f2d-3ea4-4f48-9739-174413b10dea
# Image processing function
function process_images(path)
    img = load(path)
    img = Gray.(img)
	img = resizeImage(img) 
    img = vec(img)
    img = convert(Array{Float64,1},img)
    return img
end

# ╔═╡ f10374ff-7a11-458a-9f77-d786424a3ead
md"Building the model"
model = Chain(
        Conv((3, 3), 1=>32, pad=(1,1), relu),
        MaxPool((2,2)),
        Conv((3, 3), 32=>32, pad=(1,1), relu),
       # MaxPool((2,2)),
        #Conv((3, 3), 64=>128, pad=(1,1), relu),
        MaxPool((2,2)),
        #flatten,
        Dense(500, 120, relu), 
        Dense(120, 84, relu), 
        Dense(84, 2),
        softmax)

# ╔═╡ b77dba8b-1e66-413f-a460-068bd5077c46
function fit!(model::Flux.Chain, features, labels; silent=true, max_epochs=100_000, 	conv_atol=0.005, conv_period=5)
    function loss(x, y) 
        ŷ = model(x)
        -sum(log.(ifelse.(y, ŷ, 1-ŷ))) 
    end
    
    old_loss_o = Inf
    conv_timer = conv_period   
    function stop_cb(ii,loss_val)
        loss_o = loss_val/length(labels) 
        if loss_o < old_loss_o - conv_atol
            conv_timer = conv_period
        else
            conv_timer-=1
            if conv_timer < 1
                return true
            end
        end
        old_loss_o = loss_o
        return false
    end
    
    log_cb(ii, loss_val) = println("at $ii loss: ", loss_val/length(labels))
    
    opt = ADAM(params(model))
    dataset = Base.Iterators.repeated((features, labels), max_epochs)
    Flux.train!(loss, dataset, opt,
        log_cb = Flux.throttle(silent ? (i,l) -> () : log_cb, 10), 
        stopping_criteria = iter_throttle(stop_cb, 100), 
    )
    
    model
end

# ╔═╡ 986861a4-5eef-471d-889b-7868fbc6d79d
md"# Creating the loss, accuracy functions"

# ╔═╡ f44461f7-4e92-4457-a3de-ffb360900618
begin
    train_loss = Float64[]
    test_loss = Float64[]
    acc = Float64[]
    ps = Flux.params(model)
    opt = ADAM()
    L(x, y) = Flux.crossentropy(model(x), y)
    L((x,y)) = Flux.crossentropy(model(x), y)
    accuracy(x, y, f) = mean(Flux.onecold(f(x)) .== Flux.onecold(y))
    
    function update_loss!()
        push!(train_loss, mean(L.(tryDataSet)))
        push!(test_loss, mean(L(test_set)))
        push!(acc, accuracy(test_set..., model))
        @printf("train loss = %.2f, test loss = %.2f, accuracy = %.2f\n", 	 tryDataSet[end], test_loss[end], acc[end])
    end
end

# ╔═╡ 22394c54-99b9-4d64-9fe5-a7a87f79d972
begin
    plot(train_loss, xlabel="Iterations", title="Model Training", label="Train loss", lw=2, alpha=0.9)
    plot!(test_loss, label="Test loss", lw=2, alpha=0.9)
    plot!(acc, label="Accuracy", lw=2, alpha=0.9)
end

# ╔═╡ 98da2030-f55a-4749-ba29-e02bf97504d3
opt_2 = ADAM(0.0005)

# ╔═╡ 1ffc5e2e-6575-4b1c-a30a-c23d1766b97b
@epochs 5 Flux.train!(L, ps, tryDataSet, opt_2;
               cb = Flux.throttle(update_loss!, 8))

# ╔═╡ 77677ffc-3a3f-4448-805c-9ea37496a079
model(tryDataSet)

# ╔═╡ bb7bb6b8-26fe-4ee7-a3fe-0d004a9c95ab
# Checking for model accuracy
begin
	epochs = 10
	
	for epoch = 1:epochs
  		for d in tryDataSet
    		gs = gradient(params(model)) do
      		l = loss(d...)
	   	end
    	update!(opt, params(model), gs)
	end
  	@show accuracy(valX, valY)
end
end

# ╔═╡ Cell order:
# ╠═6c8afd46-e038-4e50-bcbe-964768d7014b
# ╠═37766d90-b434-11ec-17f6-4f777d3d2564
# ╠═31034f1d-aa87-4bb9-b93c-db662ee7d27a
# ╠═43489228-8517-411c-9cd7-e4ae876cfe7f
# ╠═8ad31f6d-ff3f-495e-99f3-4a6d60a221b7
# ╠═6d57ba6c-c336-4ccb-ac19-23b8f61d4c1f
# ╠═ab465a7c-faa0-4ebd-8a72-7c5054b8d437
# ╠═1feca470-10ce-4ebe-b703-aab3cbec5ca1
# ╠═9c2aadfa-a581-436e-9160-815baa883394
# ╠═0cc2fd45-b87e-4fda-90dd-ffa537a375e9
# ╠═bef0c8aa-2de3-47fd-96c5-08e3b0cb4ea6
# ╠═eec55f2d-3ea4-4f48-9739-174413b10dea
# ╠═f10374ff-7a11-458a-9f77-d786424a3ead
# ╠═b77dba8b-1e66-413f-a460-068bd5077c46
# ╠═986861a4-5eef-471d-889b-7868fbc6d79d
# ╠═f44461f7-4e92-4457-a3de-ffb360900618
# ╠═22394c54-99b9-4d64-9fe5-a7a87f79d972
# ╠═98da2030-f55a-4749-ba29-e02bf97504d3
# ╠═1ffc5e2e-6575-4b1c-a30a-c23d1766b97b
# ╠═77677ffc-3a3f-4448-805c-9ea37496a079
# ╠═bb7bb6b8-26fe-4ee7-a3fe-0d004a9c95ab
